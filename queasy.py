#!/usr/bin/env python2.7
from cnt import cnt


class fake_module:
    def f(self):
        print('fake')

def mock_import(mod_name, *args, **kwargs):
    if mod_name == 'module':
        return fake_module()
    else:
        return real_import(mod_name, *args, **kwargs)


import __builtin__
real_import = __builtin__.__import__
__builtin__.__import__ = mock_import

cnt.cnt()
